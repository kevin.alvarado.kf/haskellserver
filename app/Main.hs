{-# LANGUAGE OverloadedStrings #-}

module Main (main) where

import Control.Monad (forever)
import qualified Data.ByteString.Char8 as BC
import Network.Socket
import Network.Socket.ByteString (recv, send)
import System.IO (BufferMode (..), hSetBuffering, stdout)
import qualified Data.String as DS

reqResponse :: Int -> BC.ByteString -> BC.ByteString -> BC.ByteString
reqResponse status headers body =
    let
        strStatusNumber = 
            DS.fromString $ show status
        strStatus = 
            case status of
                200 -> "OK"
                404 -> "Not Found"
                _ -> "Internal Server Error"

    in
    "HTTP/1.1 " <> strStatusNumber <> " " <> strStatus <> "\r\n"<> headers <> "\r\n" <> body

constResponse404 :: BC.ByteString
constResponse404 = reqResponse 404 "" ""

response200 :: BC.ByteString -> BC.ByteString
response200 body =
    let 
        strLength = DS.fromString $ show $ BC.length body
        headers = "Content-Type: text/plain\r\nContent-Length: " <> strLength <> "\r\n"
    in
    reqResponse 200  headers body

getResponse :: BC.ByteString -> BC.ByteString
getResponse path = case tail $ BC.splitWith (=='/') path of
    [""] ->
        response200 ""
    ["echo"] ->
        constResponse404
    "echo" : endPath ->
        response200 $ BC.intercalate "/" endPath
    _ ->
        constResponse404

main :: IO ()
main = do
    hSetBuffering stdout LineBuffering

    -- Uncomment this block to pass first stage
    let host = "127.0.0.1"
        port = "4221"

    BC.putStrLn $ "Listening on " <> BC.pack host <> ":" <> BC.pack port

    -- Get address information for the given host and port
    addrInfo <- getAddrInfo Nothing (Just host) (Just port)

    serverSocket <- socket (addrFamily $ head addrInfo) Stream defaultProtocol

    -- Set options
    setSocketOption serverSocket ReuseAddr 1
    withFdSocket serverSocket setCloseOnExecIfNeeded

    bind serverSocket $ addrAddress $ head addrInfo
    listen serverSocket 5

    -- Accept connections and handle them forever
    forever $ do
        (clientSocket, clientAddr) <- accept serverSocket
        BC.putStrLn $ "Accepted connection from " <> BC.pack (show clientAddr) <> "."
        req <- recv clientSocket 1024
        let response = case BC.words req of
                (_ : path : headers) ->
                    "HTTP/1.1 200 OK\r\n\r\n" <> (DS.fromString $ show headers)
                    --getResponse path
                _ ->
                    constResponse404

        _ <- send clientSocket response
        -- Handle the clientSocket as needed...

        close clientSocket
