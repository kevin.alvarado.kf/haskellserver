append2Until :: Int -> [Int]
append2Until a =
    take a
    $ iterate (\x -> x * 10 + 2) 2

main :: IO ()
main = do
    putStrLn "Type a number: "
    a <- readLn
    putStrLn "\nList result is: "
    print (append2Until a)

{-
Type a number: 
5

List result is: 
[2,22,222,2222,22222]
-}

