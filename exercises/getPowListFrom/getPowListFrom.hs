{-| Pow each number until a.
-}
getPowListFrom :: Int -> [Int]
getPowListFrom a =
    [ x^2 | x <- [1..a]]

main :: IO ()
main = do
    putStrLn "Type a number: "
    a <- readLn
    putStrLn "Pow list is: "
    print (getPowListFrom a)

{-
Type a number: 
9
Pow list is: 
[1,4,9,16,25,36,49,64,81]
-}
