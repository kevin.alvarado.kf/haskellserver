createHtml :: String -> String -> String
createHtml title content =
    "<html><head><title>" <> title <> "</title></head><body>" <> content <> "</body></html>"

main :: IO ()
main =
    do
    putStrLn "Enter the title of the page: "
    newTitle <- getLine
    putStrLn "Enter the content of the page: "
    newContent <- getLine
    putStrLn (createHtml newTitle newContent)

{-
Enter the title of the page: 
title
Enter the content of the page: 
Hello <strong>World</strong>!!!
<html><head><title>title</title></head><body>Hello <strong>World</strong>!!!</body></html>
-}

