| command | description |
| ------- | ----------- |
| ghc     | builds hs file |
| runghc     | builds hs file and execute |
| ghci     | builds hs file, and generates methods and start interface |
