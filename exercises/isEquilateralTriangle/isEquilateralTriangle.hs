checkTriangle :: Double -> Double -> Double -> String
checkTriangle a b c
    | a == b && b == c = "Equilateral"
    | a == b || b == c || a == c = "Isosceles"
    | otherwise = "Scalene"

main :: IO ()
main = do
    putStrLn "Enter the sides of the triangle: "
    a <- readLn
    b <- readLn
    c <- readLn
    putStrLn $ checkTriangle a b c

